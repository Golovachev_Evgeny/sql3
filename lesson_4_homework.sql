--����� ��: https://docs.google.com/document/d/1NVORWgdwlKepKq_b8SPRaSpraltxoMg2SIusTEN6mEQ/edit?usp=sharing
--colab/jupyter: https://colab.research.google.com/drive/1j4XdGIU__NYPVpv74vQa9HUOAkxsgUez?usp=sharing

--task13 (lesson3)
--������������ �����: ������� ������ ���� ��������� � ������������� � ��������� ���� �������� (pc, printer, laptop). �������: model, maker, type
select model, maker, type
from
(
		select pc.model, maker, product.type
		from pc
		join product
		on pc.model = product.model
	union all
		select printer.model, maker, product.type
		from printer
		join product
		on printer.model = product.model
	union all
		select laptop.model, maker, product.type
		from laptop
		join product
		on laptop.model = product.model
)	a


--task14 (lesson3)
--������������ �����: ��� ������ ���� �������� �� ������� printer ������������� ������� ��� ���, � ���� ���� ����� ������� PC - "1", � ��������� - "0"
select *,
case
    when price >(select avg(price) from PC)
    then 1
    else 0
end flag
from printer

--task15 (lesson3)
--�������: ������� ������ ��������, � ������� class ����������� (IS NULL)
select *
from Outcomes o 
full join ships s
on o.ship = s.name
where class is null


--task16 (lesson3)
--�������: ������� ��������, ������� ��������� � ����, �� ����������� �� � ����� �� ����� ������ �������� �� ����.
select name
from battles
where extract(year from date) not in
     (select launched
      from ships
      where launched is not null)
      
--task17 (lesson3)
--�������: ������� ��������, � ������� ����������� ������� ������ Kongo �� ������� Ships.

select distinct battle
from outcomes o  
left join ships s
on o.ship = s.name
where class = 'Kongo'

--task1  (lesson4)
-- ������������ �����: ������� view (�������� all_products_flag_300) ��� ���� ������� (pc, printer, laptop) � ������, ���� ��������� ������ > 300. �� view ��� �������: model, price, flag
create view pall_products_flag_300 as
(
select model, price, flag
from
(
		select model, price,
		case 
			when price < 300 
			then 1 
			else 0 
		end flag
		from pc
	union 
		select model, price,
		case 
			when price < 300 
			then 1 
			else 0 
		end flag
		from laptop
	union 
		select model, price,
		case 
			when price < 300 
			then 1 
			else 0 
		end flag
		from printer
)	a
)


--task2  (lesson4)
-- ������������ �����: ������� view (�������� all_products_flag_avg_price) ��� ���� ������� (pc, printer, laptop) � ������, ���� ��������� ������ c������ . �� view ��� �������: model, price, flag
create view all_products_flag_avg_price as
(
select model, price, flag
from
(
		select model, price,
		case 
			when price < (select avg(price) from pc)
			then 1 
			else 0 
		end flag
		from pc
	union all
		select model, price,
		case 
			when price < (select avg(price) from laptop) 
			then 1 
			else 0 
		end flag
		from laptop
	union all
		select model, price,
		case 
			when price < (select avg(price) from printer)  
			then 1 
			else 0 
		end flag
		from printer
)	a
)

--task3  (lesson4)
-- ������������ �����: ������� ��� �������� ������������� = 'A' �� ���������� ���� ������� �� ��������� ������������� = 'D' � 'C'. ������� model

SELECT p.model
FROM printer p
left join product p2 
on p.model = p2.model
where maker IN (
SELECT maker
FROM product
WHERE maker='A'
)
AND
price IN (
SELECT price
FROM printer
WHERE price > (
  	SELECT avg(price)
	FROM printer p
	left join product p2 
	on p.model = p2.model
	WHERE maker = 'D' or maker = 'C' 
	)
)


--task4 (lesson4)
-- ������������ �����: ������� ��� ������ ������������� = 'A' �� ���������� ���� ������� �� ��������� ������������� = 'D' � 'C'. ������� model
select a.model
from
(
select product.model, price
from product
join printer
on product.model=printer.model
where maker = 'A'
union 
select product.model, price
from product
join pc
on product.model=pc.model
where maker = 'A'
union 
select product.model, price
from product
join laptop
on product.model=laptop.model
where maker = 'A'
)a
WHERE price > (
  	SELECT avg(price)
	FROM printer p
	left join product p2 
	on p.model = p2.model
	WHERE maker = 'D' or maker = 'C' 
	)


--task5 (lesson4)
-- ������������ �����: ����� ������� ���� ����� ���������� ��������� ������������� = 'A' (printer & laptop & pc)
select avg(price)
from
(
select DISTINCT p2.model, price
from product p1
join printer p2
on p1.model=p2.model
where maker = 'A'
union 
select DISTINCT p2.model, price
from product p1
join pc p2
on p1.model=p2.model
where maker = 'A'
union 
select DISTINCT p2.model, price
from product p1
join laptop p2
on p1.model=p2.model
where maker = 'A'
)a
	
	
--task6 (lesson4)
-- ������������ �����: ������� view � ����������� ������� (�������� count_products_by_makers) �� ������� �������������. �� view: maker, count
create view count_products_by_makers as
(
select count(a.model), a.maker  
from (
SELECT maker, l.model
FROM laptop l 
join product p 
on l.model = p.model
union all
SELECT maker, pc.model
FROM pc 
join product p 
on pc.model = p.model
union all
SELECT maker, p2.model
FROM printer p2 
join product p 
on p2.model = p.model
) a
group by a.maker
)

--task7 (lesson4)
-- �� ����������� view (count_products_by_makers) ������� ������ � colab (X: maker, y: count)
request = """
select maker, count
from count_products_by_makers
"""
df = pd.read_sql_query("select * from count_products_by_makers", conn)
fig = px.bar(x=df.maker.to_list(), y=df['count'].to_list(), labels={'x':'maker', 'y':'count'})
fig.show()

--task8 (lesson4)
-- ������������ �����: ������� ����� ������� printer (�������� printer_updated) � ������� �� ��� ��� �������� ������������� 'D'
CREATE TABLE printer_updated AS
TABLE printer

DELETE FROM printer_updated using product
  WHERE printer_updated.model=product.model and maker = 'D'
 

--task9 (lesson4)
-- ������������ �����: ������� �� ���� ������� (printer_updated) view � �������������� �������� ������������� (�������� printer_updated_with_makers)
create view printer_updated_with_makers as
(
select pu.code, pu.model, pu.color, pu.type, pu.price, p.maker 
from printer_updated pu
inner JOIN product p 
ON pu.model = p.model
)

  
--task10 (lesson4)
-- �������: ������� view c ����������� ����������� �������� � ������� ������� (�������� sunk_ships_by_classes). �� view: count, class (���� �������� ������ ���/IS NULL, �� �������� �� 0)
create view sunk_ships_by_classes as 
(
select sum(ship)count, class
from
(
select count(ship)ship, Classes.class
 from classes right join
 Outcomes o ON Classes.class = o.ship 
 join Battles b
 on o.battle =b.name
 where o.result='sunk'
 group by Classes.class   
union 
select count(ship)ship, class
 FROM Ships LEFT JOIN 
 Outcomes ON ship=name
 where result = 'sunk'
 group by class
union 
select count(ship)ship, Ships.class
 FROM Ships right join 
 Outcomes o ON Ships.class = o.ship 
 join Battles b
 on o.battle =b.name
 where o.result='sunk'
 group by Ships.class
) a
group by class
);

CREATE TABLE sunk_ships_by_classes_update 
as TABLE sunk_ships_by_classes
update sunk_ships_by_classes_update
SET class = '0'  
WHERE class is NULL


--task11 (lesson4)
-- �������: �� ����������� view (sunk_ships_by_classes) ������� ������ � colab (X: class, Y: count)
request = """
select *
from sunk_ships_by_classes_update
"""
df = pd.read_sql_query("select * from sunk_ships_by_classes_update", conn)
fig = px.bar(x=df['class'].to_list(), y=df['count'].to_list(), labels={'x':'class', 'y':'count'})
fig.show()


--task12 (lesson4)
-- �������: ������� ����� ������� classes (�������� classes_with_flag) � �������� � ��� flag: ���� ���������� ������ ������ ��� ����� 9 - �� 1, ����� 0
CREATE TABLE classes_with_flag AS
TABLE classes
select *,
case 
	when numGuns < 9
	then 1 
	else 0 
end flag
from classes_with_flag

--task13 (lesson4)
-- �������: ������� ������ � colab �� ������� classes � ����������� ������� �� ������� (X: country, Y: count)
request = """
select country, count(class)
from classes
GROUP by country
"""
df = pd.read_sql_query("select country, count(class) from classes GROUP by country", conn)
fig = px.bar(x=df.country.to_list(), y=df['count'].to_list(), labels={'x':'country', 'y':'count'})
fig.show()

--task14 (lesson4)
-- �������: ������� ���������� ��������, � ������� �������� ���������� � ����� "O" ��� "M".
SELECT count(name) 
FROM Ships
WHERE name LIKE 'O%' or name LIKE 'M%' 

--task15 (lesson4)
-- �������: ������� ���������� ��������, � ������� �������� ������� �� ���� ����.
SELECT count(name) 
FROM Ships
WHERE name LIKE '% %' 

--task16 (lesson4)
-- �������: ��������� ������ � ����������� ���������� �� ���� �������� � ����� ������� (X: year, Y: count)
request = """
select launched, count(name)
from Ships
GROUP by launched
"""
df = pd.read_sql_query("select launched, count(name) from Ships GROUP by launched", conn)
fig = px.bar(x=df.launched.to_list(), y=df['count'].to_list(), labels={'x':'launched', 'y':'count'})
fig.show()



